use actix::prelude::*;

use diesel;
use diesel::result::Error;
use models::*;

use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use schema;

pub struct DbExecutor(pub Pool<ConnectionManager<PgConnection>>);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub struct CreateUser {
    pub username: String,
    pub email: String,
    pub password: String,
    pub difficulty: i32,
}

impl Message for CreateUser {
    type Result = Result<User, Error>;
}

impl Handler<CreateUser> for DbExecutor {
    type Result = Result<User, Error>;

    fn handle(&mut self, msg: CreateUser, _: &mut Self::Context) -> Self::Result {
        let connection = &self.0.get().unwrap();
        let new_user = NewUser {
            username: msg.username,
            email: msg.email,
            password: msg.password,
            password_difficulty: msg.difficulty,
        };
        diesel::insert_into(schema::users::table)
            .values(&new_user)
            .get_result(connection)
    }
}

pub struct GetUser {
    pub username: String,
}

impl Message for GetUser {
    type Result = Result<User, Error>; 
}

impl Handler<GetUser> for DbExecutor {
    type Result = Result<User, Error>; 

    fn handle(&mut self, msg: GetUser, _: &mut Self::Context) -> Self::Result {
        use self::schema::users::dsl::*;
        let connection = &self.0.get().unwrap();
        users.filter(username.eq(msg.username)).get_result(connection)
    }
}

pub struct CreatePost {
    pub title: String,
    pub link: String,
}

impl Message for CreatePost {
    type Result = Result<Post, Error>;
}

impl Handler<CreatePost> for DbExecutor {
    type Result = Result<Post, Error>;

    fn handle(&mut self, msg: CreatePost, _: &mut Self::Context) -> Self::Result {
        let connection = &self.0.get().unwrap();
        let new_post = NewPost {
            title: msg.title,
            link: msg.link,
        };
        diesel::insert_into(schema::posts::table)
            .values(&new_post)
            .get_result(connection)
    }
}

pub struct GetPosts {}

impl Message for GetPosts {
    type Result = Result<Vec<Post>, Error>;
}

impl Handler<GetPosts> for DbExecutor {
    type Result = Result<Vec<Post>, Error>;

    fn handle(&mut self, _: GetPosts, _: &mut Self::Context) -> Self::Result {
        use self::schema::posts::dsl::posts;
        let connection = &self.0.get().unwrap();
        posts.load(connection)
    }
}

table! {
    posts (id) {
        id -> Int4,
        title -> Varchar,
        link -> Varchar,
        createdat -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
        createdat -> Timestamp,
        password_difficulty -> Int4,
    }
}

allow_tables_to_appear_in_same_query!(
    posts,
    users,
);

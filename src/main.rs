#[macro_use]
extern crate serde_derive;
extern crate actix;
extern crate actix_web;
extern crate bcrypt;
extern crate diesel;
extern crate dotenv;
extern crate futures;
extern crate seenit;
extern crate serde;
extern crate serde_json;

use actix::prelude::*;
use actix_web::{http, server, App, AsyncResponder, Form, HttpRequest, HttpResponse, Responder};
use diesel::prelude::*;
use diesel::r2d2;
use diesel::r2d2::ConnectionManager;
use futures::future::Future;
use seenit::db::*;
use seenit::models;
use std::env;

#[derive(Deserialize, Debug)]
struct UserData {
    username: String,
    password: String,
}

struct State {
    db: Addr<Syn, DbExecutor>,
}

fn new_user((form, req): (Form<UserData>, HttpRequest<State>)) -> impl Responder {
    let hashed_password = match bcrypt::hash(&form.password, 4) {
        Ok(hash) => hash,
        // Err(e) => return Box::new(futures::future::FutureResult::from(Err(e))),
        Err(_e) => unimplemented!(),
    };
    let create_user_msg = CreateUser {
        username: form.username.clone(),
        email: "alexanderp@trulia.com".to_owned(),
        password: hashed_password,
        difficulty: 4,
    };
    req.state()
        .db
        .send(create_user_msg)
        .and_then(|res| match res {
            Ok(user) => Ok(HttpResponse::Ok().json(user)),
            Err(_) => Ok(HttpResponse::InternalServerError().into()),
        })
        .responder()
}

// Login endpoint that accepts a username and password, and currently does
// nothing with them. Should return a token or something if the credentials are
// valid, and an error message if they are not, but hey who actually cares.
fn login((form, req): (Form<UserData>, HttpRequest<State>)) -> impl Responder {
    req.state()
        .db
        .send(GetUser {
            username: form.username.clone(),
        })
        .and_then(|res| match res {
            Ok(user) => Ok(HttpResponse::Ok().json(check_user(form.into_inner(), user))),
            Err(e) => match e {
                diesel::result::Error::NotFound => Ok(HttpResponse::Unauthorized().into()),
                e => {
                    eprintln!("Could not fetch user: {}", e);
                    Ok(HttpResponse::BadRequest().into())
                }
            },
        })
        .responder()
}

fn check_user(login: UserData, user: models::User) -> bool {
    println!("Login: {:?}, User: {:?}", login, user);
    match bcrypt::verify(&login.password, &user.password) {
        Ok(result) => result,
        Err(e) => {
            eprintln!("Error hashing input password: {}", e);
            false
        }
    }
}

// Index page that greets a user when they clearly have no idea where they're
// supposed to be going.
fn index(_: HttpRequest<State>) -> impl Responder {
    "Index page thing!\n"
}

// Provides a list of all available posts, without any sort of ordering or
// filtering. God help you if you actually make this request and there are
// a lot of posts. I mean that'd be a nice problem to have, but it's unlikely
// amirite?
fn posts(req: HttpRequest<State>) -> impl Responder {
    req.state()
        .db
        .send(GetPosts {})
        .and_then(|res| match res {
            Ok(posts) => Ok(HttpResponse::Ok().json(posts)),
            Err(e) => {
                eprintln!("{:?}", e);
                Ok(HttpResponse::InternalServerError().into())
            }
        })
        .responder()
}

// Create a new post. Accepts a title and link, and everything else is
// generated automatically. If the data could not be saved we return a
// 500 Error, because who knows why it didn't actually work.
fn new_post((post, req): (Form<seenit::models::NewPost>, HttpRequest<State>)) -> impl Responder {
    req.state()
        .db
        .send(CreatePost {
            title: post.title.clone(),
            link: post.link.clone(),
        })
        .and_then(|res| match res {
            Ok(post) => Ok(HttpResponse::Accepted().json(post)),
            Err(e) => {
                eprintln!("{:?}", e);
                Ok(HttpResponse::InternalServerError().into())
            }
        })
        .responder()
}

fn not_found(_: HttpRequest<State>) -> impl Responder {
    "No page found, sorry about that"
}

fn get_connection_pool() -> r2d2::Pool<ConnectionManager<PgConnection>> {
    dotenv::dotenv().ok();
    let url = env::var("DATABASE_URL").expect("Set env DATABASE_URL");
    let manager = ConnectionManager::<diesel::pg::PgConnection>::new(url);
    r2d2::Pool::new(manager).expect("Failed to create PG connection pool")
}

fn main() {
    let sys = actix::System::new("seenit");
    let pool = get_connection_pool();
    let addr = SyncArbiter::start(8, move || DbExecutor(pool.clone()));
    server::new(move || {
        App::with_state(State { db: addr.clone() })
            .route("/", http::Method::GET, index)
            .route("/users", http::Method::POST, new_user)
            .route("/login", http::Method::POST, login)
            .route("/posts", http::Method::GET, posts)
            .route("/post", http::Method::POST, new_post)
            .default_resource(|r| r.f(not_found))
    }).bind("127.0.0.1:8080")
        .unwrap()
        .start();
    let _ = sys.run();
}

pub mod db;
pub mod models;
pub mod schema;

extern crate actix;
extern crate actix_web;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;
